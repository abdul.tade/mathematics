#include <time.h>
#include <mutex>
#include <tuple>

namespace Platform
{

    namespace Linux
    {

#if defined(__linux__)

        using TimePoint = timespec;
        using TimeInterval = double;
        using TimeSample = std::tuple<TimeInterval, TimeInterval, TimeInterval>;

        enum ClockType : int
        {
            RealTime = CLOCK_REALTIME,
            ProcessCpuTime = CLOCK_PROCESS_CPUTIME_ID,
            ThreadCpuTime = CLOCK_THREAD_CPUTIME_ID
        };

        TimeInterval operator-(const TimePoint &lhs, const TimePoint &rhs)
        {
            return (lhs.tv_sec-rhs.tv_sec) + 1e-9*(lhs.tv_nsec-rhs.tv_nsec);
        }

        struct Timer
        {
            template <typename Function, typename... Args>
            static TimeSample sample(Function &&func, Args &&...args)
            {
                auto timeSlice = [](TimePoint &rt, TimePoint &pt, TimePoint &tt) {
                    clock_gettime(ClockType::RealTime, &rt);
                    clock_gettime(ClockType::ProcessCpuTime, &pt);
                    clock_gettime(ClockType::ThreadCpuTime, &tt);
                };

                TimePoint rt0, rt1, pt0, 
                          pt1, tt0, tt1; /*rt realtime, pt process time, tt thread time*/
                
                timeSlice(rt0, pt0, tt0);
                func(std::forward<Args>(args)...);
                timeSlice(rt1, pt1, tt1);

                return { rt1 - rt0, pt1 - pt0, tt1 - tt0 };
            }

            Timer(ClockType type)
                : type_(type)
            {
            }

            void setClockType(ClockType type) noexcept
            {
                std::lock_guard<std::mutex> lock{mtx_};
                type_ = type;
            }

            template <typename Function, typename... Args>
            double timeIt(Function &&func, Args &&...args)
            {
                TimePoint start, end;
                clock_gettime(type_, &start);
                func(std::forward<Args>(args)...);
                clock_gettime(type_, &end);
                return end - start;
            }

        private:
            ClockType type_;
            std::mutex mtx_;
        };
#endif
    } // namespace Linux

} // namespace Platform
