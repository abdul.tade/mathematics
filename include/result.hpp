#include <iostream>
#include <type_traits>
#include <concepts>
#include "is_same.hpp"

template <typename T>
concept ExceptionType = requires(T &t) {
    typename std::enable_if<
        std::is_base_of_v<std::exception, T>, T>::type;
};

template <class T>
concept DefaultCopyable = requires {
    typename std::enable_if<
        std::is_default_constructible_v<T>, T>::type;
    typename std::enable_if<
        std::is_copy_constructible_v<T>, T>::type;
};

template <class T, class U>
struct Either
{

    using Left = T;
    using Right = U;

    Either(T left)
        : leftSet_(true)
    {
        left_ = new T;
        *left_ = left;
    }

    Either(U right)
        : rightSet_(true)
    {
        right_ = new U(right);
    }

    Either(const Either &other)
        : leftSet_(other.leftSet_),
          rightSet_(other.rightSet_)
    {
        left_ = new T(other.left_);
        right_ = new U(other.right_);
    }

    ~Either() noexcept
    {

        if (left_ != nullptr)
        {
            delete left_;
        }

        if (right_ != nullptr)
        {
            delete right_;
        }
    }

    Either operator=(const Either &other)
    {
        Either res(other);
        return res;
    }

    T &getLeft() const
    {
        if (not leftSet_)
        {
            throw std::runtime_error{"Cannot get left value, its not being set"};
        }
        return *left_;
    }

    U &getRight() const
    {
        if (not rightSet_)
        {
            throw std::runtime_error{"Cannot get left value, its not being set"};
        }
        return *right_;
    }

private:
    T *left_;
    U *right_;
    bool leftSet_{false};
    bool rightSet_{false};
};

// template <class T, class U>
// struct either<true, T, U> {
//     using type = T;
// };

// template <class T, class U>
// struct either<false, T, U> {
//     using type = U;
// };

template <class T, ExceptionType E>
class Result
{
public:
    template <class U>
    Result(U u)
    {
        value_ = new Either<T, E>(u);
        isValue_ = is_same_v<U, T>;
    }

    ~Result() noexcept
    {
        if (value_ != nullptr)
        {
            delete value_;
        }
    }

    bool isError() const {
        return not isValue_;
    }

    T &getValue() const
    {
        if (not isValue_)
        {
            throw std::runtime_error{"Cannot access value object, its not set"};
        }
        return value_->getLeft();
    }

    E &getError() const
    {
        if (isValue_)
        {
            throw std::runtime_error{"Cannot access error object, its not set"};
        }
        return value_->getRight();
    }

private:
    Either<T, E> *value_;
    bool isValue_{false};
};