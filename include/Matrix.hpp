#include <iostream>
#include <cmath>
#include "number.hpp"
#include "enable_if.hpp"
#include "is_integer.hpp"

namespace maths
{

using Dim = int;
static constexpr Dim Dynamic = -1;

template <Number T, Dim rows, Dim cols,class Allocator = std::allocator<T>>
class Matrix 
{
public:
    Matrix()
    {
        if constexpr ((rows < 0) or (cols < 0)) {
            static_assert(((rows != Dynamic)) & (cols != Dynamic), "Matrix dimensions that are set to dynamic must be passed to constructor");
        }
        static_assert((rows != 0) && (cols != 0), "Matrix dimensions cannot be zero");
        static_assert((rows > 0) && (cols > 0), "Matrix dimensions cannot be negative");
        mapAlloc(rows_, cols_);
    }

    Matrix(Dim _rows, Dim _cols)
        : rows_(_rows),
          cols_(_cols)
    {
        if ((_rows <= 0) and (_cols <= 0)) {
            throw std::invalid_argument{"Invalid matrix dimensions"};
        }
    }

    std::pair<Dim, Dim> dim()
    {
        return std::pair{rows_, cols_};
    }

    static Matrix random(Dim _rows, Dim _cols) {
        Matrix mat(_rows, _cols);
        for (int i = 0; i < _rows; ++i)
        {
            for (int j = 0; j < _cols; j++) 
            {
                mat.data_[i][j] = rand() % 32767;
            }
        }
        return mat;
    }



private:
    Dim rows_ = rows;
    Dim cols_ = cols;
    T** data_;
    Allocator alloc_;

    /**
     * @brief It allocates the memory and maps it to
     * two dimensional matrix
    */
    void mapAlloc(Dim _rows, Dim _cols) 
    {
        T* mem = alloc_.allocate(_rows * _cols);
        data_ = new T*[cols_];

        for (int i = 0; i < _rows; ++i)
        {
            data_[i] = mem + (i * _rows);
        }
    }
};

}