#pragma once

#include "is_primitive.hpp"


template <typename T>
concept Number = requires(T &t) {
    typename is_primitive_t<T>;
    t + t;
    t * t;
    t - t;
    t / t;
};