#pragma once

#include <concepts>

template <typename T>
struct is_integer
{
    static constexpr bool value = false;
};

template <>
struct is_integer<int>
{
    static constexpr bool value = true;
    using type = int;
};

template <>
struct is_integer<unsigned int>
{
    static constexpr bool value = true;
    using type = unsigned int;
};

template <>
struct is_integer<long>
{
    static constexpr bool value = true;
    using type = long;
};

template <>
struct is_integer<unsigned long>
{
    static constexpr bool value = true;
    using type = unsigned long;
};

template <>
struct is_integer<long long>
{
    static constexpr bool value = true;
    using type = long long;
};

template <>
struct is_integer<unsigned long long>
{
    static constexpr bool value = true;
    using type = unsigned long long;
};

template <typename T>
static constexpr bool is_integer_v = is_integer<T>::value;

template <typename T>
using is_integer_t = typename is_integer<T>::type;

template <class T>
concept Integer = requires(T &t) {
    typename is_integer<T>::type;
};