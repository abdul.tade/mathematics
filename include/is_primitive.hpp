#pragma once

template <typename T>
struct is_primitive {
    static constexpr bool value = false;
};

template<>
struct is_primitive<bool> {
    static constexpr bool value = true;
    using type = bool;
};

template <>
struct is_primitive<char> {
    static constexpr bool value = true;
    using type = char;
};

template <>
struct is_primitive<unsigned char> {
    static constexpr bool value = true;
    using type = unsigned char;
};

template <>
struct is_primitive<int> {
    static constexpr bool value = true;
    using type = int;
};

template <>
struct is_primitive<unsigned int> {
    static constexpr bool value = true;
    using type = unsigned int;
};

template <>
struct is_primitive<long> {
    static constexpr bool value = true;
    using type = long;
};

template <>
struct is_primitive<unsigned long> {
    static constexpr bool value = true;
    using type = unsigned long;
};

template <>
struct is_primitive<long long> {
    static constexpr bool value = true;
    using type = long long;
};

template <>
struct is_primitive<unsigned long long> {
    static constexpr bool value = true;
    using type = unsigned long long;
};

template <>
struct is_primitive<float> {
    static constexpr bool value = true;
    using type = float;
};

template <>
struct is_primitive<double> {
    static constexpr bool value = true;
    using type = double;
};

template <typename T>
static constexpr bool is_primitive_v = is_primitive<T>::value;

template <typename T>
using is_primitive_t = typename is_primitive<T>::type;