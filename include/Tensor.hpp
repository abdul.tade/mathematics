#include <iostream>
#include <array>
#include <tuple>
#include <concepts>
#include "is_integer.hpp"

template <class T>
struct add_pointer
{
    using type = T *;
};

template <class T, size_t n>
struct multi_pointer
{
    using type = multi_pointer<typename add_pointer<T>::type, n - 1>::type;
};

template <class T>
struct multi_pointer<T, 0>
{
    using type = T;
};

template <typename T>
concept Multiplierable = requires(T &t) {
    t *t;
};

template <Multiplierable T, T... Ts>
struct multiply
{
    static constexpr T value = (Ts * ...); /* C++ fold expressions */
};

template <class T, size_t... Dims>
class Tensor
{
public:
    Tensor()
    {
        T *d = allocate();
    }

    ~Tensor()
    {
        if (ptr_ != nullptr)
        {
            delete ptr_;
        }
    }

    template <Integral... Ts>
    auto operator()(Ts... ns)
    {
        using pointer_type = typename multi_pointer<T, sizeof...(Ts)>::type;

        pointer_type ptr{nullptr};

        if constexpr ((sizeof...(Ts) > sizeof...(Dims)) or sizeof...(Ts) == 0) {
            static_assert(false, "index exceeds dimensions");
        }
    }

    auto dim()
    {
        return std::make_tuple(Dims...);
    }

    auto data() const
    {
        return ptr_;
    }

    size_t ndim() const
    {
        return sizeof...(Dims);
    }

private:
    size_t dim_[sizeof...(Dims)] = {Dims...};
    typename multi_pointer<
        T, sizeof...(Dims)>::type ptr_;

    T *allocate()
    {
        auto size = multiply<size_t, Dims...>::value;
        return new T[size];
    }

    void map(T *ptr)
    {
    }
};