#include <iostream>
#include "../include/result.hpp"

size_t strlen(const char *str)
{
    size_t c = 0;
    const char *arr = str;
    while (arr[c++])
        ;
    return c;
}

using floating_point_exception = std::invalid_argument;

Result<double, floating_point_exception> divide(double x, double y) {
    if (y == 0) {
        return {std::invalid_argument{"division by zero not allowed"}};
    } else {
        return {x / y};
    }
}

int main()
{
    auto result = divide(10.0 , 0);

    if (result.isError()) {
        throw result.getError();
    } else {
        std::cout << result.getValue() << '\n';
    }
}