#pragma once
#include <iostream>
#include <cmath>
#include "number.hpp"
#include "is_integer.hpp"

constexpr size_t VectorAlignment()
{
    return 256ul;
}

namespace maths
{

    template <Number T, typename Allocator = std::allocator<T>>
    class Vector
    {
    public:
        using slice_type = std::pair<size_t, size_t>;

        struct iterator
        {
            using iterator_category = std::random_access_iterator_tag;
            using value_type = T;
            using pointer = T *;
            using reference = T &;
            using difference_type = ptrdiff_t;

            iterator(T *ptr)
                : ptr_(ptr)
            {
            }

            reference operator*()
            {
                return *ptr_;
            }

            pointer operator->()
            {
                return ptr_;
            }

            iterator &operator++()
            {
                ++ptr_;
                return *this;
            }

            iterator operator++(int)
            {
                auto tmp = *this;
                ++(*this);
                return tmp;
            }

            iterator &operator--()
            {
                --ptr_;
                return *this;
            }

            iterator operator--(int)
            {
                auto tmp = *this;
                --(*this);
                return tmp;
            }

            iterator operator+(ptrdiff_t offset)
            {
                return iterator{ptr_ + offset};
            }

            iterator operator-(ptrdiff_t offset)
            {
                return iterator{ptr_ + offset};
            }

            ptrdiff_t operator-(const iterator &it)
            {
                return static_cast<ptrdiff_t>(ptr_ - it.ptr_);
            }

            iterator &operator+=(ptrdiff_t offset)
            {
                ptr_ += offset;
                return *this;
            }

            iterator &operator-=(ptrdiff_t offset)
            {
                ptr_ -= offset;
                return *this;
            }

            std::strong_ordering operator<=>(const iterator &it) const noexcept = default;

        private:
            T *ptr_;
        };
        /**
         * use for places that accepts forward iterators
         * */
        iterator begin()
        {
            return iterator{data_};
        }
        /**
         * use for places that accepts forward iterators
         * */
        iterator end()
        {
            return iterator{data_ + size_};
        }

        /**
         * used by decrementable iterator
         */
        iterator rbegin()
        {
            return iterator{data_ + size_ - 1};
        }

        /**
         * used by decrementable iterators
         */
        iterator rend()
        {
            return iterator{data_ - 1};
        }

        Vector(size_t n)
        {
            if (n == 0)
            {
                throw std::invalid_argument{"vector size must be non-zero"};
            }
            capacity_ = align(n, VectorAlignment());
            size_ = 0;
            data_ = alloc_.allocate(capacity_);
        }

        Vector(const T *ptr, size_t n)
        {
            if (ptr == nullptr)
            {
                std::invalid_argument{"buffer pointer must be non-null"};
            }

            if (n == 0)
            {
                std::invalid_argument{"vector size must be non-zero"};
            }

            capacity_ = align(n, VectorAlignment());
            size_ = n;
            data_ = alloc_.allocate(capacity_);
            std::copy(ptr, ptr + n, data_);
        }

        Vector(const std::initializer_list<T> &list)
        {
            if (list.size() == 0)
            {
                capacity_ = VectorAlignment();
                size_ = 0;
            }
            else
            {
                capacity_ = align(list.size(), VectorAlignment());
                size_ = list.size();
            }

            data_ = alloc_.allocate(capacity_);
            std::copy(list.begin(), list.end(), data_);
        }

        Vector()
            : size_(0),
              capacity_(VectorAlignment())
        {
            data_ = alloc_.allocate(VectorAlignment());
        }

        ~Vector() noexcept
        {
            if (data_ != nullptr)
            {
                alloc_.deallocate(data_, capacity_);
            }
        }

        Vector(const Vector &v)
            : size_(v.size_),
              capacity_(v.capacity_)
        {
            data_ = alloc_.allocate(capacity_);
            std::copy(v.data_, v.data_ + v.size_, data_);
        }

        Vector(Vector &&v)
            : size_(v.size_),
              capacity_(v.capacity_),
              data_(v.data_)
        {
            v.size_ = 0;
            v.capacity_ = 0;
            v.data_ = nullptr;
        }

        Vector &operator=(const Vector &v)
        {
            if (&v == this)
            {
                return *this;
            }
            else if (size_ == v.size_)
            {
                std::copy(v.data_, v.data_ + v.size_, data_);
            }
            else
            {
                alloc_.deallocate(data_, capacity_);
                size_ = v.size_;
                capacity_ = v.capacity_;
                data_ = new T[capacity_];
                std::copy(v.data_, v.data_ + v.size_, data_);
            }

            return *this;
        }

        Vector &operator=(Vector &&v)
        {
            if (&v == this)
            {
                return *this;
            }
            size_ = v.size_;
            capacity_ = v.capacity_;
            data_ = v.data_;

            v.size_ = 0;
            v.capacity_ = 0;
            v.data_ = nullptr;

            return *this;
        }

        T &operator[](size_t index)
        {
            if (index > size_ - 1)
            {
                throw std::out_of_range{"index out of range"};
            }
            return data_[index];
        }

        /*Slice begins from 0 to size()-1*/
        Vector operator[](const slice_type &slice)
        {
            if (slice.first > (size_ - 1) or slice.second > (size_ - 1))
            {
                throw std::out_of_range{"slice index out of range"};
            }

            if (slice.first == slice.second)
            {
                return Vector();
            }

            return Vector(data_ + slice.first, (slice.second - slice.first));
        }

        Vector slice(size_t start, size_t end)
        {
            return operator[](slice_type{start, end});
        }

        bool operator==(const Vector &v)
        {
            if (size_ != v.size_)
            {
                return false;
            }
            return std::equal(data_, data_ + size_, v.data_, v.data_ + v.size_);
        }

        bool operator!=(const Vector &v)
        {
            return not operator==(v);
        }

        uint64_t id() const
        {
            return static_cast<uint64_t>(this);
        }

        const T *data() const
        {
            return data_;
        }

        size_t size() const
        {
            return size_;
        }

        size_t capacity() const
        {
            return capacity_;
        }

        void resize(size_t newSize)
        {
            size_t newCapacity = align(newSize, VectorAlignment());
            if (newCapacity == capacity_)
            {
                return;
            }
            auto newData = alloc_.allocate(newCapacity);
            size_t sizeToCopy = (newSize > size_) ? size_ : newSize;
            std::copy(data_, data_ + sizeToCopy, newData);
            size_ = newSize;
            alloc_.deallocate(data_, capacity_);
            capacity_ = newCapacity;
            data_ = newData;
        }

        void reserve(size_t n)
        {
            resize(size_ + n);
        }

        void clear()
        {
            T *arr = data_;
            size_t c = 0;
            while ((c < size_) and !(*(arr + (c++)) = 0))
                ;
        }

        /**
         * @brief add value to the vector
        */
        void push_back(const T &value)
        {
            if (size_ == capacity_)
            {
                size_t oldSize = size_;
                resize(size_ + VectorAlignment());
                size_ = oldSize;
            }
            data_[size_] = value;
            size_++;
        }

        T pop_back()
        {
            T retVal = data_[size_ - 1];

            if (size_ == 0)
            {
                throw std::invalid_argument{"cannot pop out of an empty vector"};
            }

            size_t alignment = VectorAlignment();

            if (((capacity_ - size_) % alignment) == 0)
            {
                if (capacity_ != VectorAlignment())
                {
                    resize(capacity_ - size_);
                }
            }
            size_ -= 1;
            return retVal;
        }

        void concat(const Vector &v)
        {
            resize(size_ + v.size_);
            std::copy(v.data_, v.data_ + v.size_, data_ + size_);
            size_ = size_ + v.size_;
        }

        [[nodiscard]] Vector operator*(const Vector &v)
        {
            if (size_ != v.size_)
            {
                throw std::invalid_argument{"Cannot multiply vectors of unequal dimensions"};
            }

            Vector res(size_);

            for (size_t i = 0; i < size_; i++)
            {
                res.data_[i] = data_[i] * v.data_[i];
            }

            res.size_ = size_;

            return res;
        }

        [[nodiscard]] Vector operator*(T factor)
        {
            Vector res(data_, size_);
            std::for_each(res.data_, res.data_ + size_, [&](T &x)
                          { x = x * factor; });
            return res;
        }

        friend Vector operator*(T factor, const Vector &v)
        {
            return v * factor;
        }

        [[nodiscard]] Vector operator+(const Vector &v)
        {
            if (size_ != v.size_)
            {
                throw std::invalid_argument{"Cannot add vectors of unequal dimensions"};
            }

            Vector res(size_);

            for (size_t i = 0; i < size_; i++)
            {
                res.data_[i] = data_[i] + v.data_[i];
            }

            res.size_ = size_;

            return res;
        }

        [[nodiscard]] Vector operator+(T factor)
        {
            Vector res(data_, size_);
            std::for_each(res.data_, res.data_ + size_, [&](T &x)
                          { x = x + factor; });
            return res;
        }

        friend Vector operator+(T factor, const Vector &v)
        {
            return v + factor;
        }

        [[nodiscard]] Vector operator-(const Vector &v)
        {
            if (size_ != v.size_)
            {
                throw std::invalid_argument{"Cannot subtract vectors of unequal dimensions"};
            }

            Vector res(size_);

            for (size_t i = 0; i < size_; i++)
            {
                res.data_[i] = data_[i] - v.data_[i];
            }

            res.size_ = size_;

            return res;
        }

        [[nodiscard]] Vector operator-(T factor)
        {
            Vector res(data_, size_);
            std::for_each(res.data_, res.data_ + size_, [&](T &x)
                          { x = x - factor; });
            return res;
        }

        friend Vector operator-(T factor, const Vector &v)
        {
            Vector res(v.data_, v.size_);
            std::for_each(res.data_, res.data_ + res.size_, [&](T &x)
                          { x = factor - x; });
            return res;
        }

        [[nodiscard]] Vector operator/(const Vector &v)
        {
            if (size_ != v.size_)
            {
                throw std::invalid_argument{"Cannot divide vectors of unequal dimensions"};
            }

            Vector res(size_);

            for (size_t i = 0; i < size_; i++)
            {
                res.data_[i] = data_[i] / v.data_[i];
            }

            res.size_ = size_;

            return res;
        }

        [[nodiscard]] Vector operator/(T factor)
        {
            Vector res(data_, size_);
            std::for_each(res.data_, res.data_ + size_, [&](T &x)
                          { x = x / factor; });
            return res;
        }

        friend Vector operator/(T factor, const Vector &v)
        {
            Vector res(v.data_, v.size_);
            std::for_each(res.data_, res.data_ + res.size_, [&](T &x)
                          { x = factor / x; });
            return res;
        }

        [[nodiscard]] Vector operator^(T power)
        {
            Vector res(data_, size_);
            std::for_each(res.data_, res.data_ + res.size_, [&](T &x)
                          { x = std::pow(x, power); });
            return res;
        }

        friend Vector operator^(T power, const Vector &v)
        {
            Vector res(v.data_, v.size_);
            std::for_each(res.data_, res.data_ + res.size_, [&](T &x)
                          { x = std::pow(power, x); });
            return res;
        }

        double norm(size_t l = 1)
        {

            if (l == 1)
            {
                return size_;
            }

            double sum = 0.0;
            std::for_each(data_, data_ + size_, [&](T x) mutable
                          {
                double tmpX = static_cast<double>(x);
                double tmpL = static_cast<double>(l);
                sum += std::pow(tmpX,tmpL); });

            return std::pow(sum, 1.0 / l);
        }

        static Vector linspace(T start, T end, T delta = 1)
        {
            Vector res;
            size_t i = 0;

            if (delta == 0) {
                throw std::invalid_argument{"increment size cannot be zero"};
            }

            auto comparator = [&](T _curr, T _end) -> bool {
                if (delta < 0) {
                    return _curr > _end;
                } else {
                    return _curr < _end;
                }
            };

            T acc = start + (i * delta);
            while (comparator(acc, end))
            {
                res.push_back(acc);
                i++;
                acc = start + (i * delta);
            }

            return res;
        }

        Vector exp()
        {
            Vector res(data_, size_);
            std::for_each(res.data_, res.data_ + res.size_, [&](T &x)
                          {
                double tmpX = static_cast<double>(x);
                x = std::exp(x); });
            return res;
        }

    private:
        T *data_;
        size_t size_;
        size_t capacity_;
        Allocator alloc_{};

        template <Integer I>
        I align(I toalign, I aligner)
        {
            return toalign + (aligner - (toalign % aligner));
        }
    };

}