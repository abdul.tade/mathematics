
template <bool value, class U>
struct enable_if {
};

template <class U>
struct enable_if<true, U> {
    static constexpr bool value = true;
    using type = U;
};

template <class U>
struct enable_if<false, U> {
    static constexpr bool value = false;
};